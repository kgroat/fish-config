
for path_part in $pre_path_parts
    # if not contains $path_part $PATH
        set -x PATH $path_part $PATH
    # end
end

for path_part in $path_parts
    # if not contains $path_part $PATH
        set -x PATH $PATH $path_part
    # end
end

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/kevingroat/google-cloud-sdk/path.fish.inc' ]; . '/Users/kevingroat/google-cloud-sdk/path.fish.inc'; end
