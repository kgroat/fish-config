# usage:
# ssh_add_with_password "$HOME/.ssh/${your_key}" "$your_key_password";

function ssh_add_pass_from_file -a keyfile askpass_file
    set -lx DISPLAY 'display'
    set -lx SSH_ASKPASS "$askpass_file"
    cat "$keyfile" | ssh-add - 2>&1 >/dev/null
end

function ssh_add_with_password -a keyfile password
    set -l askpass_file $config_root/tmp/(random_str 16)
    echo "echo '$password';" >$askpass_file
    chmod +x $askpass_file
    if not timeout 3 ssh_add_pass_from_file "$keyfile" "$askpass_file" 2>&1 >/dev/null
        echo "failed to load ssh key: $keyfile"
    end
    rm $askpass_file
end

function unload_ssh_helpers
    functions -e ssh_add_pass_from_file
    functions -e ssh_add_with_password
    functions -e unload_ssh_helpers
end
