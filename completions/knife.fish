
function __valueof
    eval "echo -s !\$$argv" | string split '!'
end

function __contains_mods -a singles_name doubles_name
    set cmd (commandline -opc)
    set -l singles (__valueof $singles_name)
    set -l doubles (__valueof $doubles_name)
    echo cmd:
    for s in $cmd
        echo "  - '$s'"
    end
    echo singles:
    for s in $singles
        echo "  - '$s'"
    end
    echo doubles:
    for s in $doubles
        echo "  - '$s'"
    end

    if not [ -z "$singles" ]
        and [ (count $cmd) -eq 2 ]
        and contains $cmd[2] $singles
        return 0
    end

    if not [ -z "$doubles" ]
        and [ (count $cmd) -eq 3 ]
        and contains (string join ' ' $cmd[2..3]) $doubles
        return 0
    end

    return 1
end

function __get_operating_on
    set -l cmd (commandline -opc)
    if [ (count $cmd) -ge 2 ]
        set -l full_command (string join ' ' $cmd[2..3])
        switch $full_command
            case '*'
                echo $full_command
        end
    end
end

# bootstrap
complete -f -c knife -n '__is_first_command' -a bootstrap -d 'Bootstrap a machine to run chef-client'

# client
complete -f -c knife -n '__is_first_command' -a client -d 'Manage a client'
complete -f -c knife -n '__previous_command client' -a reregister -d 'Begin tracking a client again'
complete -f -c knife -n '__previous_command client' -a bulk -d 'Manage multiple clients at once'

# client key
complete -f -c knife -n '__previous_command client' -a key -d 'Manage client keys'

# config
complete -f -c knife -n '__is_first_command' -a config -d 'Manage configuration profiles'
complete -f -c knife -n '__previous_command config' -a get -d 'Get the details of the current config profile'
complete -f -c knife -n '__previous_command config' -a get-profile -d 'Get the name of the current config profile'
complete -f -c knife -n '__previous_command config' -a list-profiles -d 'List the available current config profile(s)'
complete -f -c knife -n '__previous_command config' -a use-profile -d 'Switch to a given profile'

# configure
complete -f -c knife -n '__is_first_command' -a configure -d 'Manage chef configuration'
complete -f -c knife -n '__previous_command configure' -a client -d 'Manage chef client configuration'

# cookbook
complete -f -c knife -n '__is_first_command' -a cookbook -d 'Manage cookbooks'
complete -f -c knife -n '__previous_command cookbook' -a download -d 'Download a given cookbook'
complete -f -c knife -n '__previous_command cookbook' -a metadata -d 'Get the metadata for a given cookbook'
complete -f -c knife -n '__previous_command cookbook' -a upload -d 'Upload a cookbook'

# cookbook site
complete -f -c knife -n '__previous_command cookbook' -a site -d 'Manage cookbook sites'
complete -f -c knife -n '__previous_command "cookbook site"' -a download -d 'Download a given cookbook site'
complete -f -c knife -n '__previous_command "cookbook site"' -a install -d 'Install a given cookbook site'
complete -f -c knife -n '__previous_command "cookbook site"' -a search -d 'Search cookbook sites for a specified query'
complete -f -c knife -n '__previous_command "cookbook site"' -a share -d 'Share a given cookbook site'
complete -f -c knife -n '__previous_command "cookbook site"' -a unshare -d 'Stop sharing a given cookbook site'

# data bag
complete -f -c knife -n '__is_first_command' -a data -d 'Operate on a data bag'
complete -f -c knife -n '__previous_command data' -a bag -d 'Operate on a data bag'

# environment
complete -f -c knife -n '__is_first_command' -a environment -d 'Manage environments'
complete -f -c knife -n '__previous_command environment' -a environment -d 'Compare a few given environments'

# exec
complete -f -c knife -n '__is_first_command' -a exec -d 'Execute arbitrary ruby code in the context of a chef client'

# group
complete -f -c knife -n '__is_first_command' -a group -d 'Manage user groups'
complete -f -c knife -n '__previous_command group' -a add -d 'Add a user to a group'
complete -f -c knife -n '__previous_command group' -a destroy -d 'Destroy a group'
complete -f -c knife -n '__previous_command group' -a remove -d 'Remove a user from a group'

# job
complete -f -c knife -n '__is_first_command' -a push -d 'Run arbitrary chef code on a set of given nodes'
complete -f -c knife -n '__is_first_command; or __previous_command push' -a job -d 'Run arbitrary chef code on a set of given nodes'
complete -f -c knife -n '__previous_command job "push job"' -a output -d 'Get the stdout from a given push job'
complete -f -c knife -n '__previous_command job "push job"' -a start -d 'Run arbitrary chef code on a set of given nodes'
complete -f -c knife -n '__previous_command job "push job"' -a status -d 'Get the current status of a push job'

# node
complete -f -c knife -n '__is_first_command' -a node -d 'Manage nodes'
complete -f -c knife -n '__previous_command node' -a status -d 'Check the status of a given node'

# node environment set
complete -f -c knife -n '__previous_command node' -a environment -d 'Set the environment for a given node'
complete -f -c knife -n '__previous_command "node environment"' -a set -d 'Set the environment for a given node'

# node policy set
complete -f -c knife -n '__previous_command node' -a policy -d 'Set the policy for a given node'
complete -f -c knife -n '__previous_command "node policy"' -a set -d 'Set the policy for a given node'

# node run_list
complete -f -c knife -n '__previous_command node' -a run_list -d 'Manage the run list for a given node'
complete -f -c knife -n '__previous_command "node run_list"' -a add -d 'Add one or more roles or recipes to a node\'s run list'
complete -f -c knife -n '__previous_command "node run_list"' -a remove -d 'Remove one or more roles or recipes from a node\'s run list'
complete -f -c knife -n '__previous_command "node run_list"' -a set -d 'Set the run list for a given node'

# rehash
complete -f -c knife -n '__is_first_command' -a rehash -d 'Rehash the list of possible knife commands'

# role
complete -f -c knife -n '__is_first_command' -a role -d 'Manage roles that can be applied to nodes'

# role env_run_list
complete -f -c knife -n '__previous_command role' -a env_run_list -d 'Manage the run list of a given role in a given environment'
complete -f -c knife -n '__previous_command "role env_run_list"' -a add -d 'Add one or more roles or recipes to the run list of a role in a given environment'
complete -f -c knife -n '__previous_command "role env_run_list"' -a clear -d 'Remove all roles and recipes from the run list of a role in a given environment'
complete -f -c knife -n '__previous_command "role env_run_list"' -a remove -d 'Remove one or more roles or recipes from the run list of a role in a given environment'
complete -f -c knife -n '__previous_command "role env_run_list"' -a replace -d 'Remove one role or recipe with another in the run list of a role in a given environment'
complete -f -c knife -n '__previous_command "role env_run_list"' -a set -d 'Set the run list for a role in a given environment'

# role run_list
complete -f -c knife -n '__previous_command role' -a run_list -d 'Manage the run list of a given role'
complete -f -c knife -n '__previous_command "role env_run_list"' -a add -d 'Add one or more roles or recipes to the run list of a role'
complete -f -c knife -n '__previous_command "role env_run_list"' -a clear -d 'Remove all roles and recipes from the run list of a role'
complete -f -c knife -n '__previous_command "role env_run_list"' -a remove -d 'Remove one or more roles or recipes from the run list of a role'
complete -f -c knife -n '__previous_command "role env_run_list"' -a replace -d 'Remove one role or recipe with another in the run list of a role'
complete -f -c knife -n '__previous_command "role env_run_list"' -a set -d 'Set the run list for a role'

# user
complete -f -c knife -n '__is_first_command' -a user -d 'Manage users'
complete -f -c knife -n '__previous_command user' -a dissociate -d 'Dissociate a user'
complete -f -c knife -n '__previous_command user' -a reregister -d 'Re-register a user'

# user invite
complete -f -c knife -n '__previous_command user' -a invite -d 'Manage user invites'
complete -f -c knife -n '__previous_command "user invite"' -a add -d 'Create new user(s)'
complete -f -c knife -n '__previous_command "user invite"' -a recind -d 'Rescind user invite(s)'

# reused commands
set -g _fish_knife_create_singles \
    client environment group node role tag user vault
set -g _fish_knife_create_doubles \
    'client key' 'data bag' 'user key'
function __is_creatable
    __contains_mods _fish_knife_create_singles _fish_knife_create_doubles
end

function __knife_create
    set -l name (__get_operating_on)
    if [ -z "$name" ]
        printf 'create'
    else
        printf 'create'\t'Create a new %s' $name
    end
end

set -g _fish_knife_delete_singles \
    client environment node role tag user vault
set -g _fish_knife_delete_doubles \
    'client bulk' 'client key' 'data bag' 'node bulk' 'role bulk' 'user key'
function __is_deleteable
    __contains_mods _fish_knife_delete_singles _fish_knife_delete_doubles
end

function __knife_delete
    set -l name (__get_operating_on)
    if [ -z "$name" ]
        printf 'delete'
    else
        printf 'delete'\t'Delete %s(s)' $name
    end
end

set -g _fish_knife_edit_singles \
    client environment node role user vault
set -g _fish_knife_edit_doubles \
    'client key' 'data bag' 'user key'
function __is_editable
    __contains_mods _fish_knife_edit_singles _fish_knife_edit_doubles
end

function __knife_edit
    set -l name (__get_operating_on)
    if [ -z "$name" ]
        printf 'edit'
    else
        printf 'edit'\t'Edit a %s' $name
    end
end

set -g _fish_knife_list_singles \
    client cookbook environment group job node recipe role tag user vault
set -g _fish_knife_list_doubles \
    'client key' 'data bag' 'user key'
function __is_listable
    __contains_mods _fish_knife_list_singles _fish_knife_list_doubles
end

function __knife_list
    set -l name (__get_operating_on)
    if [ -z "$name" ]
        printf 'list'
    else
        printf 'list\tList %s(s)' $name
    end
end

set -g _fish_knife_show_singles \
    client cookbook environment node role user vault
set -g _fish_knife_show_doubles \
    'client key' 'cookbook site' 'data bag' 'user key'
function __is_showable
    __contains_mods _fish_knife_show_singles _fish_knife_show_doubles
end

function __knife_show
    set -l name (__get_operating_on)
    if [ -z "$name" ]
        printf 'show'
    else
        printf 'show'\t'Print details of a %s' $name
    end
end

complete -f -c knife -n '__is_creatable' -a '(__knife_create)'
complete -f -c knife -n '__is_deleteable' -a '(__knife_delete)'
complete -f -c knife -n '__is_editable' -a '(__knife_edit)'
complete -f -c knife -n '__is_listable' -a '(__knife_list)'
complete -f -c knife -n '__is_showable' -a '(__knife_show)'
