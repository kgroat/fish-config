
function __nps_commands
    node "$HOME/.config/fish-config/helpers/getNpsSuggestions.js"
end

complete -f -c nps -a init -d 'automatically migrate from npm scripts to nps'
complete -f -c nps -a help -d 'get information regarding a particular script'

complete -f -c nps -a "(__nps_commands)"
