
function __is_first_command
    set -l cmd (commandline -opc)
    [ (count $cmd) -eq 1 ]
end

function __allows_version
    set -l cmd (commandline -opc)

    if [ (count $cmd) -eq 2 ]
        if contains $cmd[2] version_commands exec run use as which bin rm '-'
            return 0
        end
    end

    if [ (count $cmd) -eq 1 ]
        return 0
    end

    return 1
end

function __get_versions
    n ls | gawk 'match($0, /^node\/(.+)$/, ary) {print ary[1]}'
end

complete -f -c n -n '__is_first_command' -a exec -d 'Execute command with modified PATH, so downloaded node <version> and npm first'
complete -f -c n -n '__is_first_command' -a prune -d 'Remove all downloaded versions except the installed version'
complete -f -c n -n '__is_first_command' -a '--latest' -d 'Output the latest node version available'
complete -f -c n -n '__is_first_command' -a '--lts' -d 'Output the latest LTS node version available'
complete -f -c n -n '__is_first_command' -a uninstall -d 'Remove the installed node and npm'

for cmd in run use as
    complete -f -c n -n '__is_first_command' -a $cmd -d 'Execute downloaded node <version> with [args ...]'
end
for cmd in which bin
    complete -f -c n -n '__is_first_command' -a $cmd -d 'Output path for downloaded node <version>'
end
for cmd in ls list
    complete -f -c n -n '__is_first_command' -a $cmd -d 'Output downloaded versions'
end
for cmd in ls-remote slr
    complete -f -c n -n '__is_first_command' -a $cmd -d 'Output matching versions available for download'
end
for cmd in rm '-'
    complete -f -c n -n '__is_first_command' -a $cmd -d 'Remove the given downloaded version(s)'
end
for cmd in lts stable
    complete -f -c n -n '__is_first_command' -a $cmd -d 'Install the latest LTS node release (downloading if necessary)'
end
for cmd in latest current
    complete -f -c n -n '__is_first_command' -a $cmd -d 'Install the latest node release (downloading if necessary)'
end

complete -f -c n -n '__allows_version' -a '(__get_versions)'
