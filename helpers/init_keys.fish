
function main
    if set -q SSH_KEYS_INITIALIZED
        echo "SSH keys have already been initialized." >&2
        return 1
    end

    mkdir -p "$HOME/.ssh"
    cat "$config_root/init/authorized_keys" >>"$HOME/.ssh/authorized_keys"

    set -x SSH_KEYS_INITIALIZED 'true'
    printf "\nset -x SSH_KEYS_INITIALIZED 'true'\n" >> $config_root/local/env.fish
end

main
functions -e main
