#!/usr/bin/env fish

function main
    set -l completion_dir $HOME/.config/fish/completions
    set -l some_installed false
    mkdir -p $completion_dir
    for file in $config_root/completions/* $config_root/local/completions/*
        set -l command (basename $file .fish)
        if not [ -e $completion_dir/$command.fish ]
            echo "Linking completions for '$command'"
            ln -s $file $completion_dir
            set some_installed true
        end
    end
    if $some_installed
        echo "Done!"
    else
        echo "All completions already installed"
    end
end

main
