#!/usr/bin/env fish

function main
    set -l omf_version v6

    function required_commands
        set -l missing_commands
        for cmd in $argv
            if not type -q $cmd
                set -a missing_commands $cmd
            end
        end
        if not [ -z "$missing_commands" ]
            echo "Some required commands are missing ($missing_commands)" >&2
            exit 1
        end
    end

    if set -q FISH_CONFIG_INITIALIZED
        echo "Config has already been initialized." >&2
        return 0
    end

    required_commands \
        pkg-config \
        curl \
        sudo

    _fish_install_completions

    # install oh my fish
    if not type -q omf
        echo 'Installing omf...'
        pushd $HOME/.config
            if not [ -d oh-my-fish ]
                echo 'Cloning oh-my-fish...'
                if not git clone https://github.com/oh-my-fish/oh-my-fish.git
                    echo "failed to clone oh-my-fish" >&2
                    return 1
                end
            end

            cd oh-my-fish
            echo "Checking out oh-my-fish $omf_version"
            if not git checkout $omf_version >/dev/null
                echo "Failed to checkout omf version $omf_version" >&2
                return 1
            end

            echo 'Running omf install script...'
            if not ./bin/install
                echo "failed to install oh-my-fish" >&2
                return 1
            end
        popd
    end

    # install fisher
    if not type -q fisher
        if not curl https://git.io/fisher --create-dirs -sLo ~/.config/fish/functions/fisher.fish
            echo "failed to install fisher" >&2
            return 1
        end
    end

    # install theme
    if not omf t $OMF_THEME >/dev/null
        echo "Installing $OMF_THEME theme from oh-my-fish..."
        omf install $OMF_THEME
    end

    echo 'Installing completions from fisher'
    fisher install evanlucas/fish-kubectl-completions

    # install version managers
    if not type -q asdf
        echo 'installing asdf...'
        if type -q brew
            brew install asdf
        else
            echo 'Not sure how to install asdf!' >&2
            return 1
        end
    end

    set -x FISH_CONFIG_INITIALIZED 'true'
    printf "\nset -x FISH_CONFIG_INITIALIZED 'true'\n" >>$config_root/local/env.fish
end

main
