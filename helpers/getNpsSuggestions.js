#!/usr/bin/env node

const { join } = require('path')

function printScripts (obj, path) {
    if (typeof obj === 'string') {
        console.log(`${path}\t${obj}`)
        return
    }

    if (typeof obj.script === 'string') {
        if (typeof obj.description === 'string') {
            console.log(`${path}\t${obj.description}`)
        } else {
            console.log(`${path}\t${obj.script}`)
        }
    }

    Object.keys(obj).filter(k => !((k === 'script' || k === 'description') && typeof obj[k] === 'string')).forEach(k => {
        const newPath = !path ? k : `${path}.${k}`
        printScripts(obj[k], newPath)
    })
}

function main () {
    let scripts
    try {
        scripts = require(join(process.cwd(), 'package-scripts.js')).scripts
    } catch {
        return
    }

    printScripts(scripts)
}

try {
    main()
} catch {
    return
}
