
if [ -f ~/.asdf/plugins/java/set-java-home.fish ]
    source ~/.asdf/plugins/java/set-java-home.fish
end

set -x SSH_KEY_PATH "~/.ssh/rsa_id"
set -x GPG_TTY (tty)
set -x npm_config_prefix $HOME
set -x N_PREFIX $HOME
set -x OMF_THEME bobthefish
set -x DEV_DIR '/development'
set -x DOCKER_BUILDKIT 1
set -x COMPOSE_DOCKER_CLI_BUILD 1
set -g theme_nerd_fonts yes
set -g theme_show_exit_status yes
set -g theme_title_display_process yes
set -x ANDROID_HOME ~/Library/Android/sdk

set pre_path_parts \
    $HOME/bin \
    $HOME/.local/bin \
    /opt/homebrew/bin \
    $HOME/.asdf/bin \
    $HOME/.asdf/shims

set path_parts \
    /usr/local/sbin \
    /usr/local/bin \
    $config_root/local/bin \
    $config_root/bin \
    $HOME/.cargo/bin \
    $ANDROID_HOME/tools \
    $ANDROID_HOME/platform-tools

set -x EDITOR 'code --wait'

if set -q SSH_CLIENT
    or set -q SSH_TTY
    set -x SSH 'true'
    set -x EDITOR 'vim'
else
    set -x SSH 'false'
    set -x EDITOR 'code --wait'
end
