
# rbenv
if type -q rbenv
    rbenv init - | source
    set -x PATH "$HOME/.rbenv/plugins/ruby-build/bin" $PATH
end

if type -q pyenv
    pyenv init - | source
end

if type -q asdf and type -q brew
    source (brew --prefix asdf)/libexec/asdf.fish
end
