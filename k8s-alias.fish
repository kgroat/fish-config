
# minikube
function kube-start -d 'Starts minikube'
  if ! type -q minikube
    echo 'minikube is not installed!' >&2
    return 1
  end

  set -q MINIKUBE_VERSION; or set -l MINIKUBE_VERSION (kubectl version --short | grep 'Client' | awk '{ print $3 }')
  set -q MINIKUBE_DRIVER; or set -l MINIKUBE_DRIVER 'virtualbox'

  echo "Using version $MINIKUBE_VERSION"
  echo "Using driver $MINIKUBE_DRIVER"
  minikube start --kubernetes-version="$MINIKUBE_VERSION" --driver="$MINIKUBE_DRIVER"
end

function kube-setup -d 'Sets up the minikube VM'
  if ! type -q minikube
    echo 'minikube is not installed!' >&2
    return 1
  end

  set -q MINIKUBE_VERSION; or set -l MINIKUBE_VERSION (kubectl version --short | grep 'Client' | awk '{ print $3 }')
  set -q MINIKUBE_DRIVER; or set -l MINIKUBE_DRIVER 'virtualbox'

  echo "Using version $MINIKUBE_VERSION"
  echo "Using driver $MINIKUBE_DRIVER"
  
  minikube start --kubernetes-version="$MINIKUBE_VERSION" --driver="$MINIKUBE_DRIVER"

  VBoxManage controlvm minikube natpf1 k8s-apiserver,tcp,127.0.0.1,8443,,8443
  VBoxManage controlvm minikube natpf1 k8s-docker,tcp,127.0.0.1,2376,,2376
  VBoxManage controlvm minikube natpf1 k8s-http,tcp,127.0.0.1,200,,80
  VBoxManage controlvm minikube natpf1 k8s-http2,tcp,127.0.0.1,8080,,8080
  VBoxManage controlvm minikube natpf1 k8s-dashboard,tcp,127.0.0.1,8001,,8001
  VBoxManage controlvm minikube natpf1 k8s-dashboard2,tcp,127.0.0.1,30000,,30000
end

function kube-stop -d 'Stops minikube'
  if ! type -q minikube
    echo 'minikube is not installed!' >&2
    return 1
  end

  minikube stop
end

function kube-docker -d 'Set up proper environment variables for using the minikube docker instance'
  set -gx DOCKER_TLS_VERIFY "1"
  set -gx DOCKER_HOST "tcp://127.0.0.1:2376"
  set -gx DOCKER_CERT_PATH "/Users/kgroat/.minikube/certs"
  set -gx MINIKUBE_ACTIVE_DOCKERD "minikube"
end


# general
function kube -w kubectl
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl $argv
end
function kube-logs --wraps 'kubectl logs' -d 'Get logs from a specific pod'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl logs $argv
end
function kube-desc --wraps 'kubectl describe' -d 'Describe a given resource'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl describe $argv
end
function kube-top --wraps 'kubectl top pod' -d 'Get the resource utilization of pods'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl top pod $argv
end


# context
function kube-ns --wraps 'kubectl config set-context --current --namespace' -d 'Change the current k8s namespace'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  if count $argv >/dev/null
    kubectl config set-context --current --namespace $argv
  else
    kubectl get namespace
  end
end
function kube-ns-current --wraps 'kubectl config set-context --current --namespace' -d 'Change the current k8s namespace'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  if count $argv >/dev/null
    kubectl config get-context --current --namespace
  else
    kubectl get namespace
  end
end
function kube-ctx --wraps 'kubectl config use-context' -d 'Change the current k8s cluster'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  if count $argv >/dev/null
    kubectl config use-context $argv
  else
    kubectl config get-contexts
  end
end


# interact
function kube-ssh -a podname cmd --wraps 'kubectl exec -it' -d 'Executes an interactive shell into a running pod'
  if test "$podname" = ''
    echo 'You must specify a pod name' >&2
    return 1
  end
  test "$cmd" = ''; and set cmd 'bash'

  kubectl exec -it "$podname" -- "$cmd" $argv[3..-1]
end


# list
function kube-configmap --wraps 'kubectl get configmap' -d 'List all configmaps'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl get configmap $argv
end
function kube-cron --wraps 'kubectl get cronjob' -d 'List all cronjobs'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl get cronjob $argv
end
function kube-deploy --wraps 'kubectl get deploy' -d 'List all deployments'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl get deploy $argv
end
function kube-job --wraps 'kubectl get job' -d 'List all jobs'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl get job $argv
end
function kube-ing --wraps 'kubectl get ing' -d 'List all ingress controllers'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl get ing $argv
end
function kube-pod --wraps 'kubectl get pods' -d 'List all pods'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl get pods $argv
end
function kube-promjob --wraps 'kubectl get promjob' -d 'List all promjobs'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl get promjob $argv
end
function kube-secret --wraps 'kubectl get secret' -d 'List all secrets'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl get secret $argv
end
function kube-svc --wraps 'kubectl get svc' -d 'List all services'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl get svc $argv
end


# describe
function kube-d-configmap --wraps 'kubectl describe configmap' -d 'Describe a specific configmap'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl describe configmap $argv
end
function kube-d-cron --wraps 'kubectl describe cronjob' -d 'Describe a specific cronjob'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl describe cronjob $argv
end
function kube-d-deploy --wraps 'kubectl describe deploy' -d 'Describe a specific deployment'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl describe deploy $argv
end
function kube-d-ing --wraps 'kubectl describe ing' -d 'Describe a specific ingress controller'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl describe ing $argv
end
function kube-d-pod --wraps 'kubectl describe pod' -d 'Describe a specific pod'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl describe pod $argv
end
function kube-d-promjob --wraps 'kubectl describe promjob' -d 'Describe a specific promjob'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl describe promjob $argv
end
function kube-d-secret --wraps 'kubectl describe secret' -d 'Describe a specific secret'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl describe secret $argv
end
function kube-d-svc --wraps 'kubectl describe svc' -d 'Describe a specific service'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl describe svc $argv
end


# delete
function kube-rm-configmap --wraps 'kubectl delete configmap' -d 'Delete a configmap'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl delete configmap $argv
end
function kube-rm-cron --wraps 'kubectl delete cronjob' -d 'Delete a cronjob'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl delete cronjob $argv
end
function kube-rm-deploy --wraps 'kubectl delete deploy' -d 'Delete a deployment'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl delete deploy $argv
end
function kube-rm-ing --wraps 'kubectl delete ing' -d 'Delete a ingress controller'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl delete ing $argv
end
function kube-rm-pod --wraps 'kubectl delete pod' -d 'Delete a pod'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl delete pod $argv
end
function kube-rm-promjob --wraps 'kubectl delete promjob' -d 'Delete a promjob'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl delete promjob $argv
end
function kube-rm-secret --wraps 'kubectl delete secret' -d 'Delete a secret'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl delete secret $argv
end
function kube-rm-svc --wraps 'kubectl delete svc' -d 'Delete a service'
  if not set -q theme_display_k8s_context
    set -g theme_display_k8s_context yes
  end
  kubectl delete svc $argv
end
