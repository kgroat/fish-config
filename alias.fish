
function android -a avd
    if not type -q emulator
        echo 'Please make sure the android `emulator` cli is installed and in your PATH.' >&2
        return 1
    end

    if [ -z "$avd" ]
        set -l avd_list (emulator -list-avds)
        set avd $avd_list[1]
        echo "No avd provided; using a default of $avd"
    end

    emulator -avd "$avd" "$argv[2..]"
end

function dev
    switch "$PWD"
        case "$DEV_DIR*"
            echo "Already in $DEV_DIR"
        case '*'
            echo "pushd $DEV_DIR"
            pushd $DEV_DIR
    end
end

function _npm_config
    if which node >/dev/null
        pushd (dirname (which node))/.. >/dev/null;
        set -x npm_config_prefix (echo $PWD)
        popd >/dev/null
    end
end

function config
    if not set -q SSH
        or $SSH
        switch "$PWD"
            case "$HOME/.config/fish-config*"
                echo "Already in ~/.config/fish-config"
            case '*'
                echo "pushd ~/.config/fish-config"
                pushd ~/.config/fish-config
        end
    else
        code ~/.config/fish-config
    end
end

if [ "$TERM_PROGRAM" = "Hyper" ]
    alias layout='hyperlayout'
end

function m3u8_dl -a url -a filename
    if [ -z "$url" ]
        echo "Enter m3u8 url:"
        read url
    end
    if [ -z "$filename" ]
        echo "Enter output filename:"
        read filename
    end
    # echo "Enter m3u8 link:";read link;
    # echo "Enter output filename:";read filename;
    ffmpeg -i "$url" -bsf:a aac_adtstoasc -vcodec copy -c copy -crf 50 "$filename.mp4"
end

function kill_port -a port -d 'Kills whichever process is using a given port'
    set -l pid (lsof -t -i :$port -sTCP:LISTEN)
    if [ "$pid" = "" ]
        echo 'Port not in use!' >&2
        return 1
    end
    echo "Killing pid $pid..."
    kill -9 $pid
    echo "Done!"
end

alias _ssh_keys_init="$config_root/helpers/init_keys.fish"
alias _fish_config_init="$config_root/helpers/init.fish"
alias _fish_install_completions="$config_root/helpers/init_completions.fish"

function gpg_fix -d 'Fixes gpg if it is acting up'
    gpgconf --kill gpg-agent
    gpgconf --launch gpg-agent
end

function gpg_test -d 'Tests whether gpg is working correctly'
    echo "test" | gpg --sign
end

function random_str -a length -d 'Generates a random string of given length'
    set -q length[1]
    or set length 16
    cat /dev/urandom | base64 | tr -dc '0-9a-zA-Z' | head -c$length
end

function timeout -a timeout_length -d 'Run a command, or kill it after a set timeframe'
    set -l elapsed 0
    set -l bg_status 0

    $argv[2..-1] &
    set -l bg_pid (jobs -lp)[1]
    if set -q bg_pid
        and not [ -z "$bg_pid" ]
        and kill -0 $bg_pid 2>&1 >/dev/null
        disown $bg_pid
    else
        return 0
    end

    function trap_interrupt
        kill $bg_pid
        wait $bg_pid
        functions -e trap_interrupt
        return (math 128 + $1 )
    end
    trap trap_interrupt SIGINT

    while [ $bg_status -eq 0 ]
        and [ $elapsed -lt $timeout_length ]

        sleep 1
        set elapsed (math $elapsed + 1)
        kill -0 $bg_pid 2>&1 >/dev/null
        set bg_status "$status"
    end
    if [ $bg_status -eq 0 ]
        echo "job timed out.  killing..." >&2
        kill $bg_pid
    end
    wait $bg_pid 2>&1 >/dev/null

    functions -e trap_interrupt
end

function regex -a rgx -d 'Grabs the content from stdin that matches a given regex'
    read -z -l stdin
    echo "$stdin" >&2
    set -l print '{print'
    if [ (count $argv) -lt 2 ]
        set print "$print ary[0]"
    else
        for index in $argv[2..-1]
            set print "$print ary[$index]"
        end
    end
    set print "$print}"
    set -l old_ifs "$IFS"
    set IFS ''
    set -l output (echo "$stdin" | gawk 'BEGIN { RS="\x0" }'\n"match(\$0,/$rgx/, ary) $print")
    set IFS $old_ifs
    if [ "$output" = "" ]
        return 1
    else
        echo $output
    end
end

function home-ip -d 'Grabs the ip of home'
    if [ -z $HOME_IP_URL ]
        return 1
    end
    curl --fail $HOME_IP_URL 2>/dev/null
end

function is-ip -a ip -d 'Checks whether a given string is a valid ip address'
    set -l ip_seg '[0-9]{1,3}'
    set -l ip_rgx "^($ip_seg\\.$ip_seg\\.$ip_seg\\.$ip_seg)"
    set -l result (echo $ip | regex $ip_rgx)
    [ ! -z "$result" ]
end

function ssh-home -d 'Opens an ssh tunnel to home'
    set -l home_ip (home-ip)
    if [ $status -ne 0 ]
        or not is-ip "$home_ip"
        return 1
    end
    echo "Home IP: $home_ip"
    ssh pi@$home_ip
end

alias clear='printf "\\033c"'

function dsize -a item -d 'Find the size of the given file directory'
    [ -z "$item" ] && set item '.'
    if not [ -r "$item" ]
        echo "ERR: No such file or directory '$item'"
        return 1
    end
    set -l size (du "$item" -b ^/dev/null | sort -r -n | head -n 1 | awk '{ print $1 }')
    set -l length (echo "$size" | string length)
    if [ "$length" -ge 10 ]
        echo (math -s2 "$size / 1000000000") "GB"
    else if [ "$length" -ge 7 ]
        echo (math -s2 "$size / 1000000") "MB"
    else if [ "$length" -ge 4 ]
        echo (math -s2 "$size / 1000") "KB"
    else
        echo "$size" "B"
    end
end

function shortcuts
    echo "Control-command shortcuts:"
    echo "^D - exit"
    echo "^L - clear"
    echo "^P - previous command in history"
    echo "^N - next command in history"
end

function reload_env
    echo 'reloading global env...'
    source $config_root/env.fish
    if [ -f $config_root/local/env.fish ]
        echo 'reloading local env...'
        source $config_root/local/env.fish
    end
end

function git-cleanup-branches -a remote
    # default argument values
    set -q remote[1]
    or set remote 'origin'

    git remote prune "$remote"
    set -l local_branches (git branch | grep -v '^\*' | sed -e 's/^ *//')
    set -l remote_branches (git branch -r | sed -e 's/^[^/]*\///')
    set -l non_remote_branches (comm -23 (printf %s\n $local_branches | psub) (printf %s\n $remote_branches | psub))
    git branch -D $non_remote_branches
end

if not which pbcopy >/dev/null
    if which xclip >/dev/null
        alias pbcopy='xclip -selection clipboard'
        alias pbpaste='xclip -selection clipboard -o'
    else if which xsel >/dev/null
        alias pbcopy='xsel --clipboard --input'
        alias pbpaste='xsel --clipboard --output'
    else
        echo 'If you want pbcopy fallback support, install xclip or xsel'
    end
end

_npm_config

# Completion helpers

function __is_nth_command -a n
    set -l cmd (commandline -opc)
    [ (count $cmd) -eq $n ]
end

function __is_first_command
    __is_nth_command 1
end

function __previous_command
    set -l cmd (commandline -opc)
    if [ (count $cmd) -ge 2 ]
        set -l full_command (string join ' ' $cmd[2..-1])
        if contains $full_command $argv
            return 0
        end
    end
    return 1
end

