
function noop -d 'Do nothing'
end

if not set -q FISH_PID
    set -x FISH_PID %self
end

set -xU SHELL (which fish)

set config_root (dirname (status --current-filename))
set fpath $config_root/fpath $fpath

set files \
    env.fish \
    load_path.fish \
    k8s-alias.fish \
    alias.fish \
    version_managers.fish \
    completion.fish \
    shell_fallback.fish \
    integrations.fish \
    load_ssh_keys.fish

set tmp_dir $config_root/tmp
mkdir -p $tmp_dir

for file in $files
    set -l file_path "$config_root/$file"
    set -l local_file_path "$config_root/local/$file"
    if [ -f $file_path ]
        source "$file_path"
    else
        echo "$file_path does not exist!"
    end
    if [ -f $local_file_path ]
        source "$local_file_path"
    end
end

if not set -q FISH_CONFIG_INITIALIZED
    and [ %self -eq $FISH_PID ]
    echo 'To finish initialization, please run the `_fish_config_init` command.'
end

# clean up temp dir if not empty
if count $tmp_dir/* 2>&1 >/dev/null
    rm -rf $tmp_dir/*
end

unload_ssh_helpers
